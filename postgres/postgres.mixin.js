import PgAdapter from './adapter';

export default (opts) => ({
  adapter: new PgAdapter(opts),
  started() {
    this.adapter = this.schema.adapter;

    this.adapter.init(this.broker, this);
  },
  stopped() {
    return this.adapter.disconnect();
  },
});
