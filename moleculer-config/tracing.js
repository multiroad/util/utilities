const tracing = {
  enabled: process.env.TRACING_ENABLED || false,
  exporter: [],
};

if (process.env.TRACING_JAEGER_ENABLED) {
  if (
    process.env.TRACING_JAEGER_ENDPOINT ||
    (process.env.TRACING_JAEGER_HOST && process.env.TRACING_JAEGER_PORT)
  ) {
    tracing.exporter.push({
      type: 'Jaeger',
      options: {
        endpoint: process.env.TRACING_JAEGER_ENDPOINT,
        host: process.env.TRACING_JAEGER_HOST,
        port: process.env.TRACING_JAEGER_PORT,
        sampler: {
          type: 'Const',
          options: {},
        },
        tracerOptions: {},
        defaultTags: null,
      },
    });
  } else {
    throw new Error('Jaeger tracing needs TRACING_JAEGER_ENDPOINT or TRACING_JAEGER_HOST & TRACING_JAEGER_PORT env variables');
  }
}

if (process.env.TRACING_EVENTLEGACY_ENABLED) {
  tracing.exporter.push({
    type: 'EventLegacy',
  });
}

export default tracing;
