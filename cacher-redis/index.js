// eslint-disable-next-line import/no-extraneous-dependencies
import OfficialCacher from 'moleculer/src/cachers/redis';
// eslint-disable-next-line import/no-extraneous-dependencies
import { METRIC } from 'moleculer/src/metrics';
// eslint-disable-next-line import/no-extraneous-dependencies
import _ from 'lodash';
import objectPath from 'object-path';

objectPath.getAll = (object, pathSrc, flatten) => {
  if (typeof pathSrc === 'string') {
    pathSrc = pathSrc.split('.');
  }

  const path = [...pathSrc];

  if (path[0] === '*') {
    path.unshift('path');
    object = { path: object };
  }

  const agg = flatten ? (arr) => arr : (arr) => [arr];

  if (Array.isArray(path) && path.indexOf('*') > 0) {
    return objectPath
      .get(object, path.slice(0, path.indexOf('*')), [])
      .reduce(
        (results, item) =>
          results.concat(
            agg(
              objectPath.getAll(item, path.slice(path.indexOf('*') + 1)),
              flatten,
            ),
          ),
        [],
      );
  }
  return objectPath.get(object, path);
};

export default class RedisCacher extends OfficialCacher {
  getKeys(key) {
    return this.client.keys(this.prefix + key);
  }

  cachePrefix(prefix) {
    if (!prefix) {
      return this.prefix;
    } else {
      return prefix;
    }
  }

  hSet(key, data, ttl) {
    this.metrics.increment(METRIC.MOLECULER_CACHER_SET_TOTAL);
    const timeEnd = this.metrics.timer(METRIC.MOLECULER_CACHER_SET_TIME);

    this.logger.debug(`HSET ${key}`);

    if (ttl == null) ttl = this.opts.ttl;

    let p;
    p = this.client.hmset(this.prefix + key, data);

    if (ttl) {
      p = p.then((res) =>
        this.client.expire(this.prefix + key, ttl).then(() => res),
      );
    }

    return p
      .then((res) => {
        timeEnd();
        return res;
      })
      .catch((err) => {
        timeEnd();
        throw err;
      });
  }

  hSetWithTags(
    key,
    data,
    ttl,
    tags = {},
  ) {
    const dataToSave = {
      data: this.serializer.serialize(data),
      tags: this.serializer.serialize(tags),
      created_at: +new Date(),
    };

    return this.hSet(key, dataToSave, ttl);
  }

  async hGetAll(key) {
    this.logger.debug(`HGETALL ${key}`);
    this.metrics.increment(METRIC.MOLECULER_CACHER_GET_TOTAL);
    const timeEnd = this.metrics.timer(METRIC.MOLECULER_CACHER_GET_TIME);
    return this.client.exists(this.prefix + key).then((exists) => {
      if (!exists) {
        timeEnd();
        return null;
      }

      return this.client.hgetall(this.prefix + key).then((dataSrc) => {
        this.logger.debug(`HGETALL FOUND ${key}`);
        this.metrics.increment(METRIC.MOLECULER_CACHER_FOUND_TOTAL);

        try {
          const result = {};

          Object.keys(dataSrc).forEach((key) => {
            result[key] = this.serializer.deserialize(dataSrc[key]);
          });
          timeEnd();

          return result;
        } catch (err) {
          this.logger.error('Redis result parse error.', err, dataSource);
        }

        timeEnd();
        return null;
      });
    });
  }

  hGet(key, prop) {
    this.logger.debug(`HGET ${key}`);
    this.metrics.increment(METRIC.MOLECULER_CACHER_GET_TOTAL);
    const timeEnd = this.metrics.timer(METRIC.MOLECULER_CACHER_GET_TIME);

    return this.client.hget(this.prefix + key, prop).then((dataSrc) => {
      if (dataSrc) {
        this.logger.debug(`FOUND ${key}`);
        this.metrics.increment(METRIC.MOLECULER_CACHER_FOUND_TOTAL);
        try {
          timeEnd();
          return this.serializer.deserialize(dataSrc);
        } catch (err) {
          this.logger.error('Redis result parse error.', err, dataSource);
        }
      }
      timeEnd();
      return null;
    });
  }

  middleware() {
    return (handler, action) => {
      const opts = _.defaultsDeep(
        {},
        _.isObject(action.cache) ? action.cache : { enabled: !!action.cache },
      );
      opts.lock = _.defaultsDeep(
        {},
        _.isObject(opts.lock) ? opts.lock : { enabled: !!opts.lock },
      );

      if (opts.enabled !== false) {
        const isEnabledFunction = _.isFunction(opts.enabled);

        return function cacherMiddleware(ctx) {
          if (isEnabledFunction) {
            if (!opts.enabled.call(ctx.service, ctx)) {
              // Cache is disabled. Call the handler only.
              return handler(ctx);
            }
          }

          // Disable caching with `ctx.meta.$cache = false`
          if (ctx.meta['$cache'] === false) return handler(ctx);

          const cacheKey = this.getCacheKey(
            action.name,
            ctx.params,
            ctx.meta,
            opts.keys,
          );

          const requestNewData = (data = null) => {
            const requestAndSetCache = () =>
              handler(ctx).then(async (result) => {
                const { tags: tagsFromParams } = opts;
                // Если есть теги и контент то создаем тэги для хранилища
                if (tagsFromParams && result) {
                  // Генерим теги
                  const generated = this.generateTags(result, tagsFromParams);

                  this.logger.debug('generated -> ', generated);

                  // Получаем теги из объекта
                  const tagItemFromGeneratedTags = Object.keys(generated);

                  // Получаем теги из хранилища
                  const fromStorageTags = await this.getTags(
                    tagItemFromGeneratedTags,
                  );

                  // Теги для добавления в хранилище
                  const forCacheTags = {};

                  // Проверяем какие теги нужно добавить в хранилище
                  tagItemFromGeneratedTags.forEach((t, i) => {
                    // if (!fromStorageTags[i] || fromStorageTags[i] < generated[t]) {
                    if (!fromStorageTags[i]) {
                      forCacheTags[t] = generated[t];
                    }
                  });

                  if (Object.keys(forCacheTags).length) {
                    this.setTags(forCacheTags);
                  }
                  this.hSetWithTags(cacheKey, result, opts.ttl, generated);
                } else {
                  this.hSetWithTags(cacheKey, result, opts.ttl);
                }
                return result;
              });

            if (opts.lock.enabled !== false) {
              const lockFunction = data
                ? this.tryLock.bind(this)
                : this.lock.bind(this);

              return lockFunction(cacheKey, opts.lock.ttl)
                .then((unlock) => {
                  return requestAndSetCache()
                    .then((res) => {
                      unlock();
                      return res;
                    })
                    .catch((err) => {
                      return this.del(cacheKey).then(() => {
                        unlock();
                        throw err;
                      });
                    });
                })
                .catch((/*err*/) => {
                  // The cache is refreshing on somewhere else.
                  return data;
                });
            }

            return requestAndSetCache();
          };

          // Not using lock
          return this.hGetAll(cacheKey).then(async (content) => {
            if (content != null && content.data) {
              const { data, tags: tagsFromData, created_at } = content;

              // Получаем теги из объекта
              const tagItemFromGeneratedTags = Object.keys(tagsFromData);

              // Получаем теги из хранилища
              const fromStorageTags = await this.getTags(
                tagItemFromGeneratedTags,
              );
              this.logger.debug(
                `\r\n${action.name}: Not using lock -> tagItemFromGeneratedTags`,
                tagItemFromGeneratedTags,
              );
              this.logger.debug(
                `\r\n${action.name}: Not using lock -> fromStorageTags`,
                fromStorageTags,
              );
              // Проверяем какие теги нужно добавить в хранилище
              const needReset = tagItemFromGeneratedTags.some((t, i) => {
                this.logger.debug(
                  `tagsFromData ->  ${+tagsFromData[
                    t
                    ]}; fromStorageTags -> ${+fromStorageTags[i]}`,
                );
                return (
                  !fromStorageTags[i] || fromStorageTags[i] > tagsFromData[t]
                );
              });

              if (needReset) {
                // this.logger.debug("content -> requestNewData(data)", content);
                this.logger.debug(
                  `${action.name}: content needReset -> requestNewData(data)`,
                );
                return requestNewData(data);
              }

              ctx.cachedResult = true;
              return data;
            }
            this.logger.debug(
              `${action.name}:content -> requestNewData`,
              content,
            );

            // Call the handler
            return requestNewData();
          });
        }.bind(this);
      }

      return handler;
    };
  }

  generateTags(
    content,
    configPath,
  ) {
    if (configPath && configPath.length) {
      const result = {};

      configPath.forEach((r) => {
        if (!r.idField) {
          throw Error('Cache path "Id" field not found');
        }

        let idsArray = objectPath.getAll(content, [...r.path, r.idField], true);
        if (!Array.isArray(idsArray) && (typeof idsArray === 'string' || typeof idsArray === 'number')) {
          idsArray = [idsArray];
        }

        let updatedAtArray = objectPath.getAll(
          content,
          [...r.path, r.timestampField],
          true,
        );

        if (!Array.isArray(updatedAtArray) && typeof updatedAtArray === 'string') {
          updatedAtArray = [updatedAtArray];
        }

        idsArray.forEach(
          (i, index) =>
            (result[`${r.name}:${i}`] = isNaN(+String(updatedAtArray[index]))
              ? +new Date()
              : +updatedAtArray[index]),
        );
      });
      return result;
    }
    return {};
  }

  getTags(
    tags,
    prefix = this.cachePrefix(),
  ) {
    if (!tags || !tags.length) return [];
    this.logger.debug('GET TAGS');
    return this.client.mget(tags.map((t) => `${prefix}TAG-${t}`));
  }

  setTags(
    tags,
    prefix = this.cachePrefix(),
  ) {
    this.logger.debug('SET TAGS');
    return this.client.mset(
      Object.keys(tags)
        .map((tag) => [`${prefix}TAG-${tag}`, tags[tag]])
        .flat(1),
    );
  }

  exist(
    key,
    prefix = this.cachePrefix(),
  ) {
    return !!this.client.exists(prefix + key);
  }

  _generateKeyFromObject(
    obj,
  ) {
    if (Array.isArray(obj)) {
      return '[' + obj.map(o => this._generateKeyFromObject(o)).join('|') + ']';
    } else if (_.isObject(obj)) {
      return (
        '{' +
        Object.keys(obj)
          .map(key => [key, this._generateKeyFromObject(obj[key])].join(':'))
          .join('|') +
        '}'
      );
    } else if (obj != null) {
      return obj.toString();
    } else {
      return 'null';
    }
  }
}

export const redisCacherMoleculerDbMixin = (model) => ({
  actions: {
    find: {
      cache: {
        tags: [
          {
            name: model.name,
            path: ['*'],
            idField: 'id',
          },
        ],
      },
    },
    count: {
      cache: true,
    },
    list: {
      cache: {
        tags: [
          {
            name: model.name,
            path: ['rows', '*'],
            idField: 'id',
          },
        ],
      },
    },
    create: {
      cache: false,
    },
    insert: {
      cache: false,
    },
    get: {
      cache: {
        tags: [
          {
            name: model.name,
            path: ['*'],
            idField: 'id',
          },
        ],
      },
    },
    update: {
      cache: false,
    },
    remove: {
      cache: false,
    },
  },
  methods: {
    /**
     * Clear the cache & call entity lifecycle events
     *
     * @methods
     * @param {String} type
     * @param {Object|Array<Object>|Number} json
     * @param {Context} ctx
     * @returns {Promise}
     */
    async entityChanged(type, json, ctx) {
      if (this.type === 'created') {
        await this.clearCache(); // TODO: придумать как чистить только кеш, в котором созданная запись может присутствовать
      } else {
        const ids = [].concat(json).map(x => x.id);

        const tags = {};

        ids.forEach(id => tags[`${this.model.name}:${id}`] = +new Date());

        await this.broker.cacher.setTags(tags);
      }

      return Promise.resolve().then(() => {
        const eventName = `entity${_.capitalize(type)}`;
        if (this.schema[eventName] != null) {
          return this.schema[eventName].call(this, json, ctx);
        }
      });
    },
    /**
     * Clear cached entities
     *
     * @methods
     * @returns {Promise}
     */
    clearCache() {
      this.broker[this.settings.cacheCleanEventType](`cache.clean.${this.fullName}`);
      if (this.broker.cacher) {
        return this.broker.cacher.clean(`${this.fullName}.**`);
      }
      return Promise.resolve();
    },
  },
});
