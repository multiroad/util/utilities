import crypto from 'crypto';

const secret = 'multiroad';

export default str => crypto.createHmac('sha256', secret).update(str, 'utf8').digest('hex');
