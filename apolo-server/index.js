'use strict';

const core = require('apollo-server-core');
const { ApolloServer } = require('./ApolloServer');
const ApolloService = require('./service');

module.exports = {
  // Core
  GraphQLUpload: core.GraphQLUpload,
  GraphQLExtension: core.GraphQLExtension,
  gql: core.gql,
  ApolloError: core.ApolloError,
  toApolloError: core.toApolloError,
  SyntaxError: core.SyntaxError,
  ValidationError: core.ValidationError,
  AuthenticationError: core.AuthenticationError,
  ForbiddenError: core.ForbiddenError,
  UserInputError: core.UserInputError,
  defaultPlaygroundOptions: core.defaultPlaygroundOptions,

  // GraphQL tools
  ...require('graphql-tools'),

  // Apollo Server
  ApolloServer,

  // Apollo Moleculer Service
  ApolloService,
};
