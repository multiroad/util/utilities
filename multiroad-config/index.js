const path = require('path');
const fs = require('fs');

const appDir = process.cwd();

let filePath = null;

if (!filePath && fs.existsSync(path.resolve(appDir, 'multiroad.config.js'))) {
  filePath = path.resolve(appDir, 'multiroad.config.js');
}

if (!filePath && fs.existsSync(path.resolve(appDir, 'multiroad.config.json'))) {
  filePath = path.resolve(appDir, 'multiroad.config.json');
}

let config = {};

if (filePath) {
  let defaults = require(filePath);

  if (defaults.default) defaults = defaults.default;

  config = Object.entries(defaults)
    .reduce((cfg, [key, defaultValue]) => {
      cfg[key] = process.env[key] || defaultValue;
      return cfg;
    }, {});
} else {
  throw new Error('multiroad.config.js or multiroad.config.json is not found in this project');
}

module.exports = config;
